package db

import "errors"

// Model is an interface that all types that need to access
// database CRUD capabilties.
type Model interface {
	SetID(int)
	GetID() int
	String() string
}

func Save(m Model) (bool, error) {
	// TODO: fill the functionality of this function
	return false, errors.New("The function is still under-construciton")
}

func Update(m Model, id int) (bool, error) {
	// TODO: fill the functionality of this function
	return false, errors.New("The function is still under-construciton")
}

func Delete(m Model, id int) (bool, error) {
	// TODO: fill the functionality of this function
	return false, errors.New("The function is still under-construciton")
}

func GetByID(m Model, id int) (bool, error) {
	// TODO: fill the functionality of this function
	return false, errors.New("The function is still under-construciton")
}
