package db

import (
	"testing"

	"bitbucket.org/boolow5/BolChatbot/models"

	. "github.com/franela/goblin"
)

func TestDbCRUD(t *testing.T) {
	g := Goblin(t)
	g.Describe("Testing Database Save Method", func() {
		w := models.Word{Text: "ninka"}
		OK, err := Save(&w)
		g.It("Should return OK", func() {
			g.Assert(OK).Equal(true)
		})
		g.It("Should not return error", func() {
			g.Assert(err).Equal(nil)
		})
	})
}
