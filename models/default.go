package models

type Word struct {
	ID   int
	Text string
}

type Sentence struct {
	ID    int
	Chunk string
	Words []Word
}

type Speach struct {
	Chunk     string
	Sentences []Sentence
}

func (w *Word) GetID() int {
	return w.ID
}

func (w *Word) SetID(id int) {
	w.ID = id
}

func (w *Word) String() string {
	return w.Text
}
