package models

import (
	"fmt"
	"strings"
)

// Word.Stem removes the common article suffixes that follow the Somali
// language words. Then words with the same stem but have different article
// suffixes can be compared or fetched without the need to pattern match
// using regular expressions.
// `		w := World{Text: "ninka"}
//			fmt.Println(w.Text)		// output: ninka
// 			fmt.Println(w.Stem()) // output: nin
// `
func (w *Word) Stem() string {
	// remove 'ka', 'da'
	if len(w.Text) < 3 {
		fmt.Println("word is too short")
		return w.Text
	}
	somArticles := []string{"ka", "da", "ga", "ha"}
	fmt.Println("Articles:", somArticles)
	lIndex := len(w.Text) - 1
	fmt.Printf("word length: %d\tlast index: %d\n", len(w.Text), lIndex)
	lastTwoLetters := w.Text[lIndex-1:]
	fmt.Println("last two letters", lastTwoLetters)
	for _, v := range somArticles {
		fmt.Println("v=", v)
		if lastTwoLetters == v {
			fmt.Println(lastTwoLetters, "==", v)
			return w.Text[:lIndex-1]
		}
	}
	fmt.Println("failed returning the original")
	return w.Text
}

// Sentence.Parse tries to split the Sentence.Chunk and put them in to
// Sentence.Words array, if unsuccessful it returns false otherwise true
func (s *Sentence) Parse() bool {
	// break into words
	s.Chunk = strings.Replace(s.Chunk, ",", "", -1)
	wds := strings.Split(s.Chunk, " ")
	for _, v := range wds {
		if len(v) > 0 {
			s.Words = append(s.Words, Word{Text: v})
		}
	}
	return len(s.Words) > 0
}

// Sentence.StemWords stems all words in the Words array
func (s Sentence) StemWords() bool {
	if len(s.Words) < 1 {
		return false
	}
	for i := 0; i < len(s.Words); i++ {
		s.Words[i] = Word{Text: s.Words[i].Stem()}
	}
	return true
}

// Speach.Parse is useful when you have a chunk of paragraphes or sentences
// that need to be parsed into sentences then to words.
// if the process succeeds it returns true otherwise it returns false.
func (s *Speach) Parse() bool {
	// trim spaces
	s.Chunk = strings.TrimSpace(s.Chunk)
	// break the Chunk into sentences
	sentences := strings.Split(s.Chunk, "\n")
	if len(sentences) < 1 {
		// means no chunk no sentences
		return false
	}

	// Give each sentence its chunk and add it to the Speach.Sentences
	for _, v := range sentences {
		if v != " " && v != "" {
			fmt.Println(v)
			s.Sentences = append(s.Sentences, Sentence{Chunk: v})
		}
	}

	// Parse each sentence to its Words array
	for i := 0; i < len(s.Sentences); i++ {
		s.Sentences[i].Parse()
	}

	// if no sentences and the Speach.Sentences have no items items
	return len(sentences) > 0 && len(s.Sentences) > 0
}
