package models

import (
	"testing"

	. "github.com/franela/goblin"
)

func TestWordMethods(t *testing.T) {
	g := Goblin(t)
	g.Describe("Testing Word Manipulation Functions", func() {
		w := Word{"ninka"}
		g.It("Should return the stem of the word", func() {
			g.Assert(w.Stem()).Equal("nin")
		})
	})
}

func TestSentenceMethods(t *testing.T) {
	g := Goblin(t)
	g.Describe("Testing Sentence Manipulation Functions", func() {
		s := Sentence{Chunk: "ninka aad u jeedo waa Mahdi"}
		g.It("Should parse and return true", func() {
			g.Assert(s.Parse()).Equal(true)
		})
		g.It("Should have more words after parsing", func() {
			g.Assert(len(s.Words) > 1).Equal(true)
		})
		g.It("Should stem words an return true", func() {
			g.Assert(s.StemWords()).Equal(true)
		})
		g.It("After stemming the fist word should be nin", func() {
			g.Assert(s.Words[0].Text).Equal("nin")
		})
		g.It("After stemming the last word should be Mahdi", func() {
			g.Assert(s.Words[len(s.Words)-1].Text).Equal("Mahdi")
		})
	})
}
func TestSpeachMethods(t *testing.T) {
	g := Goblin(t)
	g.Describe("Testing Speach Manipulation Functions", func() {
		ch := `
		Hi, how are you?
		I'm fine, thanks
		How are you?
		I'm OK
		What is your name?
		I'm Bolow Chatbot.
		What is your name?
		I'm Mahdi
		`
		s := Speach{Chunk: ch}
		g.It("Should parse and return true", func() {
			g.Assert(s.Parse()).Equal(true)
		})
		g.It("Should have 8 sentences", func() {
			g.Assert(len(s.Sentences)).Equal(8)
		})
	})
}
